# snounder
![full-icon](icon.png)<small>icon design using GeoGebra.</small>
## Name
<!--The name `snounder` is a Win32-styled name, which is a mixture of ""-->
## Description
This mod "fixes" the inability of accumulation of snow layer higher than 8.
## Installation
### Build
You can build this project using Apache Maven, Gradle, etc. This is a CC0
project, so the build scripts and configuration files are not provided.
### Packaging Manually
You can package these following files into a JAR, which are necessary:
#### `/fabric.mod.json`
See [fabric.mod.json](src/main/resources/fabric.mod.json).
#### `/snounder.mixins.json`
See [snounder.mixins.json](src/main/resources/snounder.mixins.json).
#### `/snounder-refmap.json`
##### (for MC 1.19.3-1.20.2)
```json
{
  "mappings": {
    "rege/pegui/snounder/mixin/SnounderMixin1": {
      "canPlaceAt": "Lnet/minecraft/class_2488;method_9558(Lnet/minecraft/class_2680;Lnet/minecraft/class_4538;Lnet/minecraft/class_2338;)Z",
      "randomTick": "Lnet/minecraft/class_2488;method_9514(Lnet/minecraft/class_2680;Lnet/minecraft/class_3218;Lnet/minecraft/class_2338;Lnet/minecraft/class_5819;)V"
    },
    "rege/pegui/snounder/mixin/SnounderMixin2": {
      "tickIceAndSnow": "Lnet/minecraft/class_3218;method_52370(ZLnet/minecraft/class_2338;)V"
    }
  },
  "data": {
    "named:intermediary": {
      "rege/pegui/snounder/mixin/SnounderMixin1": {
        "canPlaceAt": "Lnet/minecraft/class_2488;method_9558(Lnet/minecraft/class_2680;Lnet/minecraft/class_4538;Lnet/minecraft/class_2338;)Z",
        "randomTick": "Lnet/minecraft/class_2488;method_9514(Lnet/minecraft/class_2680;Lnet/minecraft/class_3218;Lnet/minecraft/class_2338;Lnet/minecraft/class_5819;)V"
      },
      "rege/pegui/snounder/mixin/SnounderMixin2": {
        "tickIceAndSnow": "Lnet/minecraft/class_3218;method_52370(ZLnet/minecraft/class_2338;)V"
      }
    }
  }
}
```
##### (for MC 1.20.3-latest)
```json
{
  "mappings": {
    "rege/pegui/snounder/mixin/SnounderMixin1": {
      "canPlaceAt": "Lnet/minecraft/class_2488;method_9558(Lnet/minecraft/class_2680;Lnet/minecraft/class_4538;Lnet/minecraft/class_2338;)Z",
      "randomTick": "Lnet/minecraft/class_2488;method_9514(Lnet/minecraft/class_2680;Lnet/minecraft/class_3218;Lnet/minecraft/class_2338;Lnet/minecraft/class_5819;)V"
    },
    "rege/pegui/snounder/mixin/SnounderMixin2": {
      "tickIceAndSnow": "Lnet/minecraft/class_3218;method_52370(Lnet/minecraft/class_2338;)V"
    }
  },
  "data": {
    "named:intermediary": {
      "rege/pegui/snounder/mixin/SnounderMixin1": {
        "canPlaceAt": "Lnet/minecraft/class_2488;method_9558(Lnet/minecraft/class_2680;Lnet/minecraft/class_4538;Lnet/minecraft/class_2338;)Z",
        "randomTick": "Lnet/minecraft/class_2488;method_9514(Lnet/minecraft/class_2680;Lnet/minecraft/class_3218;Lnet/minecraft/class_2338;Lnet/minecraft/class_5819;)V"
      },
      "rege/pegui/snounder/mixin/SnounderMixin2": {
        "tickIceAndSnow": "Lnet/minecraft/class_3218;method_52370(Lnet/minecraft/class_2338;)V"
      }
    }
  }
}
```
#### `/rege/pegui/snounder/Main.class`
Decode the following Base64 to `/rege/pegui/snounder/Main.class.xz` and extract
it:
```
/Td6WFoAAAFpIt42AgAhAQAAAAA3J5fW4AwVBRddAGU/k0vgBotERIKkE6M76tCCI7qpmOwDElot
ANhE633AispQrzoKQjwket+6mOv7BoobEqjTpftiue1vhqY3zi6I3dKeSaUp2QAK2w4Q0QKaJIVU
6nyGpmdGOgCGhl4IWCB6RQuxFg7IfCGHhG2zoEVyzOBBD3c8oUIMZiqGfl5N22to5Qt1ZO3pJl+X
gWEVmmAag8AY/F4bUDwOI18TaKBy3i2J6O364BsWrQrNux9uX1tzFa67jYJaH5p1TFovFzp5npxN
2CYF7j8tgETNQlB0TJUAse41xMFsIr+nGiMOz/lEDAglAaQbwqtWbLKsilc2xp7xaShvng95kGf6
1ZIIRhD6gmfg7Ch442MTszeV9iE7LgVMewTQ3uMGOQk4Bzp9S0ALevoYdg/gtRlSwXzpwqpPs+nY
MomPi8vMsusnPrB397hB3XJLG8Yj6RZBP+OXcpVHGJHb7hJg+MUzalaIu7jO60XpKPTLRr8InHmX
WNoKwwCP2LBZgWwR1jjK4gDNEHMmyaoskuEqt9CywBcdJK+ZryzEl1ROLNhuiCOiFk+WrSSPK+0Z
0wedUi6OxBXpKA7ogEc9n1X+Ytwy76+zcaffBNfJvh68cmjIutfIW5IwiRndCnIntf0a55HgQbuv
aAXNoNjSjA+1KR4Iqdy2aFjMw/pm/UnYT5LQqCVyVdujFuwdQ3sA8e2x2EHkhb25SN+UxRAdZmQR
zZIbc9qxLcQ15ok2VqOxBYYqsaIWeviU0dZ0AlVKyMiISbqbEfzl5jg6yKQmAL22A7Nai+zAK7gz
3zbhPQ840n6MVWy2m2nv3YLY0amVxZKMf9o7D/wEW3pmdh8pyVFiLeGvO0aH51+kcZJEPeOuOjWv
ApUFUuhiQz5pWdm7cDYUf70EiDr5iKL2rzyTWfcCQFEUIAP83nhrV1NVwo5U4MquyNB+ASUZIrl7
rApIBsiuEdJ+osdRQt76U43qDisbgYW1u/9r/FgWxDGlYtPZuvH8hKjnQJOZ1Vm+BAUcOu/48Uta
L9NAAS3ch3/WTL38iW2dy07Jc/F+bl6mFsU6MWNisONEkrXYNqx+eokPWS7Yko478SceQHQvugEy
qMUXRUXZ990iOdz8FVmvoqupL3HtcIy5lLifTjbdDs1YHuejtcSQPR5XRxHkj+XflmiwB/zBrjCp
2hpSKNb6009Hyr/Sx+nQsGMXNXjFW0p4bwVIUzfMj+SmrD21QfiehrTXyxzA7AXB1e4NOTVqETVf
LT7BdPrLipeu3GS/MmvjczDfd6eK4nKlRmoKit+bIT6aqM/N6fNRmHnzjspBDAZa4e1fUVWeOJv8
w3v1PmQxGcZuhk0Mj9UwPWII0nE0G0QiwG3a94CAuDdiWWXApq+8SURj0vZWWRkiM7h6bGM5pxId
wblxNWCn3sAoivL/dgI4n830mynJBvov0RZF3p+BXvIvXFXkwdzkA9eW6h85dZWzOawUn2Pv1Zb7
JHZ5w4UXmXxDywgui5fPkyg+KUXBDkyBC/XNZkB61vFyFu7yukl83kxZtCW8wml5wiPgyt+fXFZe
hSptKSqrc18B2f9hTTOBtGkgCjsk6GzoCvZIcmxY8m+N7pEhfomtuVIfmnlRC6Y1FdwTVAHL+dbX
Dqk5tp6feHVvpfx+iTHTGD5E6FPNLA3tgrM5VICLajiqrLL/Fu3M7/ojrQcp6i2kMSrR+2VQgLBm
/MFqBsxQFpWFCSNo0Xn9FnYY4uYy2zEAAP/oxRAAAa8KlhgAAPcjses+MA2LAgAAAAABWVo=
```
#### `/rege/pegui/snounder/mixin/SnounderMixin1.class`
Decode the following Base64 to
`/rege/pegui/snounder/mixin/SnounderMixin1.class.xz` and extract it:
```
/Td6WFoAAAFpIt42AgAhAQAAAAA3J5fW4AzkBNldAGU/k0vgBotERHj4/6U4PmeH9JFePc69UHNM
4KsNZRGBR9XBsuvLkFaUoECDL0IRvFD5iLhpCsZMUz7tzfO8NikZFax3yBSphuLzc7zvt9LfAn1c
8w0XDBbqxNSPwXKjdFCfmPigNV6VCWlPENUEhSWosxXsr+16G6PqdDgNDPmVuKzLv04SOX0nyKe9
Xc+fFPJRrzArZTPD9eVXlPZ+Y2p/Zg35qyqPb8laTcF8YqGNTw0/eXJiLn2JjKGOuLy81iEb1NOD
CjN3Viz+oMnV3MlE5mN6miN4dUCYa/9GebqDvWVwzPHuJmdpwD/0Eptax7TtanwCGDSltfjlaQc4
sONT4Kntd6rh9u1ZrtENZDSJqj0EsuWf/uHUeQiWLHT14HUKbIueYPw6xRQZ79aii21T47m7OKRD
1CK76/Iq4h3frYA1VksrQv87631C5aa9FcfVTSTFDsEKRzUhIYjj95QFsc7BiSIrhooxdZshSMGD
X4SuHeNuSx7toNd3vV7C/Qz4qkSBCLEf2ZuESLuJ8DfQzj8iOW3VPUQZ1iiK9ijNNuOJZykqboyD
PMcZCQnmdNoqhujo8mOsBc/axffv6oYPx01yvu/DocXIE8JC4MKptrHDhru38s8Bpwigc1GBLi+r
mQzTu8aQs7KA2dluweXwBowvQH5YV0BhaDQwoTIrxGKDnzhVo11EcHtfRpjbx0+aZK8CAoLNoo1a
uAVjjpGQjKYNZ4x7KFdBS+ArXRGJdlZpTB/fWTt3zfNEdMimuOR81y978/63qC25dnxFFwVefxBg
R5Eo/NCMJcrMUMMbndm2uK5jtL5GlVCz2DZgChTkrPzXRN7rQSGIxwcRMgyybvZOlDIiNzT9NH+0
b/kIzYRwhGMbeoAyXj81o/wwvswn86X/O0O7xgnC1EWuvweHE+K7wlATu0kwxW5NTUSbjgeXFNSW
pXvaCZx0aRZzO0gZn5DCSCJKn97kJag86Ls4J7VqFl14HVe2RPq8ftuwZM8FAxrbHk80Hh8ZcLLu
Tk0miexGZJ/zfxu2ttte3Ap3Mfkkaxsf4O6XJlqMu5R3XaEgwQ6v14qG6l2jgCub3WkER0sosYgM
M367jrFZirzl3aou71XOFSDRKCIPpNJkabzQWsx1PvUgWM1yF3jj4qeexjMwuc6T4S7INDjCKLw/
SVB3DQ4dmYPjq95eGlju+hNozdCbGBIWPjjc/q6uiEBdPvA0/8JdxQlu5fwnrNalYL9UNoKiDi6E
qgJSIyiKoXGjjermgJa3ph4lE0guqi7+1n0VFqzWoFNMyBIRaDyfeb3lGkJ9bdnivX6vYDceHDeI
9lheF+IuoheHKPua7CNOQ+P8vV0Or3M1QIa22Z8Pd/C+EqpurB7wELs8ERVWbhbiy9PDw6Dk6K86
hcomuwpfeekxEnvno7O+NL6YgQlgTTmFTTWsROEAC3H0VngKv8Wu2VjPPtEUh7+A1L5tcGJkDlO0
l4rLhxzTG4cm2DODaKvdS+vYkyeGEpt8FvdzEbAlCTPD9U6q4usSDyMBVd97OnBYvLco6Qj9ApX/
mEotVOnQneQtZrHY+2ws5w30OX4f2pIwufRjEgRgYUjwI55ImWOSfb95pjG+mssbAiSYdop+2zID
/pRtjncK58iJAAhziFelC4wAAAAAAFMFq3sAAfEJ5RkAAOVV3eI+MA2LAgAAAAABWVo=
```
#### `/rege/prgui/snounder/mixin/SnounderMixin2.class`
Decode the following Base64 fits your MC version to
`/rege/pegui/snounder/mixin/SnounderMixin2.class.xz` and extract it:
##### (for MC 1.20.3-latest)
```
/Td6WFoAAAFpIt42AgAhAQEAAABSQCtu4BAfBpNdAGU/k0vgBotERL1OH5mLk6xTpnMq5UOdJIMj
ACEMmh8tuaql4pMQITY4Wt4Z7V2kPH7CLvXsrX7hGY0DnbuUQJEurPpanCcSgAd5dpgBAKBEELBu
uvvUP1xp6ULUK+ks7TL4ZEZCdY7zu+Dn6mfWbCZXQoDpnrSJ3gK7g0iNf2JTBOKbZbJk83w2CWBC
LCWk69XQWHvxYIldSEEwI0qcPdoiQVrzsiPQC6mlGu1aKbHAYQMGRkUIOhvlC/Xh9wOh9qIsCuR2
GoUHbfJuB3+iEnMUYsOwitfFo1DkY/fwjTpevzk742HZt35Ix6wCoUzKFea9iDfgzIO5mzM3FO3u
E5VP4FRNuYNXFQNNU2BX/uSUbbtTVuqrkJkoDEQlvaDG0QhJwWclVya1Pd973c/5q153ku+GCdWQ
B1Rg8zvv323+Y5j6eJlyVQ1JtnNpiIG/LRMvWeZChrFONlJnCyuM+vCpPEVAG9l9Ngcsheb98hvt
oGlVlaHRPicsAjjJ0qWHT1Hi+JNvRb6eYeTlx+THoU77aGolSdDLo8DnxeduevvA1iOXyUXkt2Uw
WMJc4x1yq5DuR8ig64J4hhPu3MW680Fz3tgLVNrs55utBT4OGXp+rvi5v6/qduW6d41gzN8Msm14
aGzJsi5hcvUIO/DNvcmlqOx82TQ/rNJtEIwxi1SXlC4b+RmXd5DLFSl1/Wb4ezE7M7Q9ct3vr4V3
yDHaSfeExK6FwGYk507gg9ypRTOh6igxa8Ehk3a6yeZYwPvmX5bBqfXopD7T8gfg32cXt2g2gfR2
GGQb3PB9C6S9qC+FhghgiqcxTexLxIiIG1mdpqTuZmg+zcn1pDXROqdCIooV/J5+IS4QmojEoJK5
DLkQGqQ0FJYhsRNjJhyBB7hrAganyMP5oC0wPCbloTBR5lUHhLI3QRki8QFlqez8KW72L9lIyj1i
+mszQBS5qvozuz5kgFr/d9H8Ou+8v57JYPenh5ahzS+WAIZipeFTHxzndiV65cWUnuzIzRT3mlYB
uvwuAp2RQhwveknIDUElBemIDyOnRYP3FvDUJ3Ec4ZhYzfah0CTSeRcek3XJOfUrUEfDPnL1h6O8
9SWOa8atq7NJJ53fwiWVYcEzs2uTzuiPHrUSw5R+RVHi7JgjpwTeevplBokMcwIIXvUpOnS4F6pZ
HDDkqLmKGe/vgVO14C4pKuQcdi4swjuTQDc+qHy12vv17KIDA4EXvcI/e48ai3Vc4lw7IywQCtxF
sNvh3lSf09qenMCGPEjeIdootfnKO/M6VZr2EOBxNZpeXPiFoeUKJhyjQUL3oXwmO7u6Hd54uiz3
SY1Ngnrc4zc7l7VIZuNXFt84YA9hfVmkqjGtrkpMY5fB0TsWYNmGD4+NXJQnIzTp3X49r7rrFsNc
0G3fpbSuUUe9r8Ib0VgoJz5PhWjaUT1/8+iHaJ6Q758iHEKs5drBL902dMxlIYdhtS2p5VwCbmpI
KDoX+rvpsbo2UNhWT5qtf2OMTse4WLbLrEVF4JO1jdkuWBWx5gIukVB0nvfmpiKJw6TQfeAE2Ofa
s+E5FuncWAFlb2KqgU5PwFDu+NDUiue/FqTted5JLDcNJJ5ErZsO3cG0ZQ63gAojEr8H0way5uK/
BbbL2Eo3fkBHPZWrAF9LL3WlVh2cRpyruVPgZ7zkxHWl7FVwsm+Jh13HYHEpEFhtm+hRLXgn7FbR
TWpBbAUo92jZ9uhzI4AX8CT9aENmA1P0ZYZ+M102Sc8uE//yuwuQlO+AFxsNonm3Oy/YGFwpvZgi
oMQIMVCFEO3ak+i+BArSw+Bl60eoWrdI1hCUKfKZQuoNw2LPSUcajAzS6jwey6ptLT+R63WadFje
tgok4HwoFGH5gk4pe1NmOfa0c50aSeVVe6WCH7UmxUa2nxnbYZuO78LTxzCIRncWbIPmdp9VlV/y
+J4eFjo/uspGj3FgNwabXpBdI5/CL6Zv2lyVINDIblRj389AX6vLvzs7MDWf20qNpGrLnet5ntXP
0rciYipycgS7WnJEjX5J8UGndqmlh1chGo+9ehhT/jktIgHqQAlm54HalXpb3lAidwGnuRQk+W8E
svgq+s1Ng6c+QXZy+0H2SorZtHHTDw99cjWmngyUefAao7F94gEy6jhXYrtETfYgzSKH/Vi+OIng
mh5IW3wVrw377sQJ7IcY7hneomb3O6jx42g7QW2oUDzrrqYCzS+aXAXd6zURBIuGpeMN5o/pl7rO
eSCrAAAA/O+UxgABqw2gIAAApK44PT4wDYsCAAAAAAFZWg==
```
## Usage
Copy the JAR into `mods` directory in your gamedir.
## Support
Go to the issue page of this project.
## Contributing
Before contributing, you need to know that this project is licensed under CC0;
if you contribute to this project, that means you agree to dedicate your work
to the public domain.  
Another, do not reformat my code. Compact is my style.
## Authors and acknowledgment
* REGEdt32
## License
This software is licensed under [Creative Commons Zero 1.0
Universal](https://creativecommons.org/publicdomain/zero/1.0).
## Project status
This project will update if a new Minecraft version which all the existing mods
cannot work on release. On the rest of the time, if there is no bug, this
project will stop updating.
