/*
snounder
Written in 2024 by REGEdt32
To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.
You should have received a copy of the CC0 Public Domain Dedication along with this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/
package rege.pegui.snounder;
import static net.minecraft.block.Blocks.POWDER_SNOW;
import java.util.function.ToDoubleBiFunction;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.math.BlockPos;
public class Main implements net.fabricmc.api.ModInitializer{
	public static boolean stackSnowOverOneBlock=true;
	public static boolean snowLayerOnPowderSnowPlaceable=true;
	public static ToDoubleBiFunction<ServerWorld,java.util.Map.Entry<Integer,
	BlockPos>>stackSnowOverOneBlockProbability=(world,layersAndBasePos)->1.;
	public static ToDoubleBiFunction<ServerWorld,BlockPos>
	snowToPowderSnowProbability=(world,pos)->world.getBlockState(pos.down())
	.isOf(POWDER_SNOW)?.00390625*(world.getBlockState(pos).get(net.minecraft.block
	.SnowBlock.LAYERS).intValue()-1):(.000244140625*((world.getBlockState(pos.
	north()).isOf(POWDER_SNOW)?1:0)+(world.getBlockState(pos.south())
	.isOf(POWDER_SNOW)?1:0)+(world.getBlockState(pos.west()).isOf(POWDER_SNOW)?1:
	0)+(world.getBlockState(pos.east()).isOf(POWDER_SNOW)?1:0)));
	@Override public void onInitialize(){}
}
