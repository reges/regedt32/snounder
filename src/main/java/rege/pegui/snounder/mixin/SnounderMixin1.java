/*
snounder
Written in 2024 by REGEdt32
To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.
You should have received a copy of the CC0 Public Domain Dedication along with this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/
package rege.pegui.snounder.mixin;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import net.minecraft.block.BlockState;
import net.minecraft.util.math.BlockPos;
import rege.pegui.snounder.Main;
import static net.minecraft.block.Blocks.POWDER_SNOW;
@Mixin(net.minecraft.block.SnowBlock.class)public abstract class
SnounderMixin1{@Inject(method="canPlaceAt",at=@At("RETURN"),cancellable=true)
private void injectCanPlaceAt(BlockState state,net.minecraft.world.WorldView
world,BlockPos pos,org.spongepowered.asm.mixin.injection.callback
.CallbackInfoReturnable<Boolean>info){if(Main
.snowLayerOnPowderSnowPlaceable){BlockState bl=world.getBlockState(pos.down());
if(bl.isOf(POWDER_SNOW)&&!info.getReturnValueZ())info.setReturnValue(true);}}
@Inject(method="randomTick",at=@At("RETURN"))private void
injectRandomTick(BlockState state,net.minecraft.server.world.ServerWorld world,
BlockPos pos,net.minecraft.util.math.random.Random random,org.spongepowered.asm
.mixin.injection.callback.CallbackInfo info){if(world.getBlockState(pos)
.isOf((net.minecraft.block.SnowBlock)(Object)this)&&Math.random()<Main
.snowToPowderSnowProbability.applyAsDouble(world,pos))world.setBlockState(pos,
POWDER_SNOW.getDefaultState());}}
