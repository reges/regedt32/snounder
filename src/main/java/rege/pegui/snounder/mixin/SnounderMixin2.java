/*
snounder
Written in 2024 by REGEdt32
To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.
You should have received a copy of the CC0 Public Domain Dedication along with this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/
package rege.pegui.snounder.mixin;
import java.util.AbstractMap.SimpleImmutableEntry;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import net.minecraft.block.BlockState;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.math.BlockPos;
import rege.pegui.snounder.Main;
import static net.minecraft.block.Blocks.POWDER_SNOW;
import static net.minecraft.block.Blocks.SNOW;
import static net.minecraft.block.SnowBlock.LAYERS;
@Mixin(net.minecraft.server.world.ServerWorld.class)public abstract class
SnounderMixin2{@Inject(method="tickIceAndSnow",at=@At("HEAD"))private void
injectTickIceAndSnow(boolean raining,BlockPos pos,org.spongepowered.asm.mixin
.injection.callback.CallbackInfo info){if(Main.stackSnowOverOneBlock&&
raining){ServerWorld w=(ServerWorld)(Object)this;net.minecraft.util.math
.BlockPos basep=w.getTopPosition(net.minecraft.world.Heightmap.Type
.MOTION_BLOCKING,pos),p=basep;net.minecraft.world.biome.Biome biome=w
.getBiome(p).value();int i=w.getGameRules().getInt(net.minecraft.world
.GameRules.SNOW_ACCUMULATION_HEIGHT);int rs=i;BlockState st=w.getBlockState(p);
while((st.isOf(SNOW)&&st.get(LAYERS).intValue()==8)||st.isOf(POWDER_SNOW)){rs-=
8;p=p.up();st=w.getBlockState(p);}if(rs<i&&rs>0&&biome.canSetSnow(w,p)){if(st
.isOf(SNOW)){int j=st.get(LAYERS);if(j<Math.min(rs,8)&&Math.random()<Main
.stackSnowOverOneBlockProbability.applyAsDouble(w,new SimpleImmutableEntry<>(i-
rs+j,basep))){BlockState nst=st.with(LAYERS,j+1);net.minecraft.block.Block
.pushEntitiesUpBeforeBlockChange(st,nst,w,p);w.setBlockState(p,
nst);}}else if(Math.random()<Main.stackSnowOverOneBlockProbability
.applyAsDouble(w,new SimpleImmutableEntry<>(i-rs,basep))){w.setBlockState(p,
SNOW.getDefaultState());}}}}}
